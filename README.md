# NES PowerPak Mapper Accumulator

This is a collection of mapper source code for RetroUSB's PowerPak. The source for these mappers is reproduced, where possible, with permission from the original developers in an attempt to organize all the source in a single location that can be easily maintained. If you'd like to add your mapper implementation, or variant thereof, please submit a pull request or request to become a contributor.

## Organization

In order to keep things easy to find, this repo has been organized into directories as follows:


| Folder Name | Description |
| ------ | ------ |
| NNN_[author] | Mapper folder, with a 3 digit number followed by an underscore and then the author's name. Contains the verilog source for the mapper. |
| common | Common files needed to build the mappers | 
| output | Folder containing all the mapper binary files for the PowerPak mappers. This will include mappers that do not have their source currently in the repo. |
| output_[author] | Significant forks from the mappers will be collected in a separate output folder under the author's name |

