`default_nettype none

module MAPBE(   //signal descriptions in powerpak.v
    input m2,
    input m2_n,
    input clk20,

    input reset,
    input nesprg_we,
    output nesprg_oe,
    input neschr_rd,
    input neschr_wr,
    input [15:0] prgain,
    input [13:0] chrain,
    input [7:0] nesprgdin,
    input [7:0] ramprgdin,
    output [7:0] nesprgdout,

    output [7:0] neschrdout,
    output neschr_oe,

    output chrram_we,
    output chrram_oe,
    output wram_oe,
    output wram_we,
    output prgram_we,
    output prgram_oe,
    output [18:10] ramchraout,
    output [18:13] ramprgaout,
    output reg irq,
    output ciram_ce,
    output exp6,
    
    input cfg_boot,
    input [18:12] cfg_chrmask,
    input [18:13] cfg_prgmask,
    input cfg_vertical,
    input cfg_fourscreen,
    input cfg_chrram
);

    reg [3:0] prgbank_reg;
	 reg [6:0] chrbank_reg [3:0];
	 
    always@(posedge m2) begin
        if(nesprg_we & prgain[15]) begin
            case({prgain[14:13]})
                0: prgbank_reg <= {1'b0, nesprgdin[2:0]}; // 8-9
                1: chrbank_reg[prgain[1:0]] = nesprgdin;  // a-b
                2: prgbank_reg <= {1'b1, nesprgdin[2:0]}; // c-d
            endcase
        end
    end

//bankswitch
    reg [18:13] prgbank_now;
    always@(prgain,prgbank_reg)
        case({prgain[14:13]})
            0:prgbank_now=(prgbank_reg<<1);
            1:prgbank_now=(prgbank_reg<<1)|1;
            2:prgbank_now=0;
            3:prgbank_now=1;
        endcase

    reg [18:10] chrbank;
    always@(cfg_fourscreen,cfg_vertical,chrain,chrbank_reg[0],chrbank_reg[1],chrbank_reg[2],chrbank_reg[3]) begin
        chrbank[18]=chrain[13];
        if(!chrain[13]) begin
				case({chrain[12:10]})
					0: chrbank[17:10] = {chrbank_reg[0],chrain[10]};
					1: chrbank[17:10] = {chrbank_reg[0],chrain[10]};
					2: chrbank[17:10] = {chrbank_reg[1],chrain[10]};
					3: chrbank[17:10] = {chrbank_reg[1],chrain[10]};
					4: chrbank[17:10] = {chrbank_reg[2],chrain[10]};
					5: chrbank[17:10] = {chrbank_reg[2],chrain[10]};
					6: chrbank[17:10] = {chrbank_reg[3],chrain[10]};
					7: chrbank[17:10] = {chrbank_reg[3],chrain[10]};
				endcase
        end else begin
            chrbank[17:12]=0;
            chrbank[11]=chrain[11] & cfg_fourscreen;
            chrbank[10]=((cfg_vertical & chrain[11]) | (~cfg_vertical & chrain[10]));
        end
    end

//rom size mask
    assign ramprgaout[18:13]=(prgbank_now[18:13] & cfg_prgmask & {4'b1111, {2{prgain[15]}}});
    assign ramchraout=chrbank & {1'b1, cfg_chrmask[17:12], 2'b11};

//bus control
    assign ciram_ce=0;
    assign chrram_we=neschr_wr & (cfg_chrram | chrain[13]);
    assign chrram_oe=neschr_rd;

    assign prgram_oe= ~cfg_boot & m2_n & ~nesprg_we & prgain[15];
    assign wram_oe=               m2_n & ~nesprg_we & prgain[15:13]==3'b011;

    assign wram_we=m2_n & nesprg_we & prgain[15:13]=='b011;

    assign prgram_we=0;

    wire config_rd;
    gamegenie gg(m2, reset, nesprg_we, prgain, nesprgdin, ramprgdin, nesprgdout, config_rd);

    assign nesprg_oe=wram_oe | prgram_oe | config_rd;

    assign neschr_oe=0;
    assign neschrdout=0;

//sound
    assign exp6=1'bz;

endmodule