`default_nettype none

module MAP6F (   //signal descriptions in powerpak.v
    input m2,
    input m2_n,
    input clk20,

    input reset,
    input nesprg_we,
    output nesprg_oe,
    input neschr_rd,
    input neschr_wr,
    input [15:0] prgain,
    input [13:0] chrain,
    input [7:0] nesprgdin,
    input [7:0] ramprgdin,
    output [7:0] nesprgdout,

    output [7:0] neschrdout,
    output neschr_oe,

    output chrram_we,
    output chrram_oe,
    output wram_oe,
    output wram_we,
    output prgram_we,
    output prgram_oe,
    output [18:10] ramchraout,
    output [18:13] ramprgaout,
    output reg irq,
    output ciram_ce,
    output exp6,
    
    input cfg_boot,
    input [18:12] cfg_chrmask,
    input [18:13] cfg_prgmask,
    input cfg_vertical,
    input cfg_fourscreen,
    input cfg_chrram
);

    reg [3:0] prgbank_reg;
    reg chrbank_reg;
    reg namebank_reg;
	 
    always@(posedge m2) begin // register mask: 01x1 xxxx xxxx xxxx
        if(nesprg_we & prgain[12] & prgain[14] & !prgain[15]) begin
            prgbank_reg <= nesprgdin[3:0];
            chrbank_reg <= nesprgdin[4];
            namebank_reg <= nesprgdin[5];
        end
    end

//rom size mask
    assign ramprgaout[18:13] = {prgbank_reg, prgain[14:13]} & cfg_prgmask & {4'b1111, {2{prgain[15]}}};
    assign ramchraout[18:10] = {4'b0000, chrain[13]?namebank_reg:chrbank_reg, chrain[12:10]};

//bus control
    assign ciram_ce=0;
    assign chrram_we=neschr_wr;
    assign chrram_oe=neschr_rd;

    assign prgram_oe= ~cfg_boot & m2_n & ~nesprg_we & prgain[15];
    assign wram_oe=               m2_n & ~nesprg_we & prgain[15:13]==3'b011;

    assign wram_we=m2_n & nesprg_we & prgain[15:13]=='b011;

    assign prgram_we=0;

    wire config_rd;
    gamegenie gg(m2, reset, nesprg_we, prgain, nesprgdin, ramprgdin, nesprgdout, config_rd);

    assign nesprg_oe=wram_oe | prgram_oe | config_rd;

    assign neschr_oe=0;
    assign neschrdout=8'bx;

//sound
    assign exp6=1'bz;

endmodule