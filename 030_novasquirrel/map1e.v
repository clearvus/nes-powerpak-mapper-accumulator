`default_nettype none

// UNROM-512

module MAP1e(     //signal descriptions in powerpak.v
    input m2,
    input m2_n,
    input clk20,

    input reset,
    input nesprg_we,
    output nesprg_oe,
    input neschr_rd,
    input neschr_wr,
    input [15:0] prgain,
    input [13:0] chrain,
    input [7:0] nesprgdin,
    input [7:0] ramprgdin,
    output [7:0] nesprgdout,

    output [7:0] neschrdout,
    output neschr_oe,

    output chrram_we,
    output chrram_oe,
    output wram_oe,
    output wram_we,
    output prgram_we,
    output prgram_oe,
    output [18:10] ramchraout,
    output [18:13] ramprgaout,
    output irq,
    output ciram_ce,
    output exp6,
    
    input cfg_boot,
    input [18:12] cfg_chrmask,
    input [18:13] cfg_prgmask,
    input cfg_vertical,
    input cfg_fourscreen,
    input cfg_chrram
);
    assign irq=0;               //no IRQs
    assign exp6=1'bz;           //no sound

    reg [4:0] prgbank;
    reg [1:0] chrbank;
    reg screenbank;    // for one-screen-mirroring mode 
    always@(posedge m2) begin
        if(nesprg_we && prgain[15]) begin
            prgbank <= nesprgdin[4:0];
            chrbank <= nesprgdin[6:5];
            screenbank <= nesprgdin[7];
        end
    end

//chr control
    assign ramchraout[10]= cfg_fourscreen ? screenbank :                                     // single screen mirroring
                           !chrain[13] ? chrain[10]: (cfg_vertical? chrain[10]: chrain[11]);  // vertical/horizontal mirroring
    assign ramchraout[18:11]= {4'b0000, chrbank, chrain[12:11]};
    assign ciram_ce=chrain[13];

//prg control
    assign ramprgaout[18:13] = (prgain[14] ? {5'b11111, prgain[13]} : {prgbank, prgain[13]}) & cfg_prgmask & {4'b1111,{2{prgain[15]}}};
//ram control
    assign chrram_we=neschr_wr & !chrain[13];
    assign chrram_oe=neschr_rd & !chrain[13];

    assign neschr_oe=0;
    assign neschrdout=8'bx;

    assign wram_oe=m2_n & ~nesprg_we & prgain[15:13]=='b011;
    assign wram_we=m2_n &  nesprg_we & prgain[15:13]=='b011;

    assign prgram_we=0;
    assign prgram_oe=~cfg_boot & m2_n & ~nesprg_we & prgain[15];

    wire config_rd;
    gamegenie gg(m2, reset, nesprg_we, prgain, nesprgdin, ramprgdin, nesprgdout, config_rd);
    assign nesprg_oe=wram_oe | prgram_oe | config_rd;

endmodule
